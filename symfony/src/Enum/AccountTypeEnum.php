<?php

namespace App\Enum;

abstract class AccountTypeEnum
{
    const TYPE_MAIN = "main";
    const TYPE_SAVING = "saving";

    /** @var array user friendly named type */
    protected static $typeName = [
        self::TYPE_MAIN => 'Principal',
        self::TYPE_SAVING => 'Economie',
    ];

    /**
     * @param string $typeShortName
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::TYPE_MAIN,
            self::TYPE_SAVING
        ];
    }
}
