<?php

namespace App\Form;

use App\Entity\Account;
use App\Enum\AccountTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add(
                'type',
                ChoiceType::class, [
                'required' => true,
                'choices' => AccountTypeEnum::getAvailableTypes(),
                'choice_label' => function($choice) {
                    return AccountTypeEnum::getTypeName($choice);
                },
            ])
            ->add('connection')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Account::class,
        ]);
    }
}
