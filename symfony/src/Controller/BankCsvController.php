<?php

namespace App\Controller;

use App\Entity\BankCsv;
use App\Form\BankCsv1Type;
use App\Repository\BankCsvRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bank/csv")
 */
class BankCsvController extends AbstractController
{
    /**
     * @Route("/", name="bank_csv_index", methods={"GET"})
     */
    public function index(BankCsvRepository $bankCsvRepository): Response
    {
        return $this->render('bank_csv/index.html.twig', [
            'bank_csvs' => $bankCsvRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bank_csv_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $bankCsv = new BankCsv();
        $form = $this->createForm(BankCsv1Type::class, $bankCsv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bankCsv);
            $entityManager->flush();

            return $this->redirectToRoute('bank_csv_index');
        }

        return $this->render('bank_csv/new.html.twig', [
            'bank_csv' => $bankCsv,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bank_csv_show", methods={"GET"})
     */
    public function show(BankCsv $bankCsv): Response
    {
        return $this->render('bank_csv/show.html.twig', [
            'bank_csv' => $bankCsv,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bank_csv_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, BankCsv $bankCsv): Response
    {
        $form = $this->createForm(BankCsv1Type::class, $bankCsv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bank_csv_index');
        }

        return $this->render('bank_csv/edit.html.twig', [
            'bank_csv' => $bankCsv,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bank_csv_delete", methods={"DELETE"})
     */
    public function delete(Request $request, BankCsv $bankCsv): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bankCsv->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bankCsv);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bank_csv_index');
    }
}
