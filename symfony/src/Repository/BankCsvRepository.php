<?php

namespace App\Repository;

use App\Entity\BankCsv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BankCsv|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankCsv|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankCsv[]    findAll()
 * @method BankCsv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankCsvRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankCsv::class);
    }

    // /**
    //  * @return BankCsv[] Returns an array of BankCsv objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankCsv
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
